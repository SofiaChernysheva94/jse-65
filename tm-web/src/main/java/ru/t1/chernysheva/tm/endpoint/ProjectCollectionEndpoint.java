package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.service.IProjectService;
import ru.t1.chernysheva.tm.model.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectCollectionEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping()
    public List<ProjectDTO> get() {
        return projectService.findAll();
    }

    @PostMapping
    public void post(@NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(projects);
    }

    @PutMapping
    public void put(@NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(projects);
    }

    @DeleteMapping()
    public void delete() {
        projectService.removeAll();
    }

}
