package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.service.ITaskService;
import ru.t1.chernysheva.tm.model.TaskDTO;

@RestController
@RequestMapping("/api/task")
public class TaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @GetMapping("/{id}")
    public TaskDTO get(@NotNull @PathVariable("id") String id) {
        return taskService.findOneById(id);
    }

    @PostMapping
    public void post(@NotNull @RequestBody TaskDTO task) {
        taskService.save(task);
    }

    @PutMapping
    public void put(@NotNull @RequestBody TaskDTO task) {
        taskService.save(task);
    }

    @DeleteMapping("/{id}")
    public void delete(@NotNull @PathVariable("id") String id) {
        taskService.removeOneById(id);
    }

}
