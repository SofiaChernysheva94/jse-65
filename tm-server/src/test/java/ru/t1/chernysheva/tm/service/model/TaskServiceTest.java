package ru.t1.chernysheva.tm.service.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.chernysheva.tm.api.service.model.ITaskService;
import ru.t1.chernysheva.tm.api.service.model.IUserService;
import ru.t1.chernysheva.tm.configuration.ServerConfiguration;
import ru.t1.chernysheva.tm.migration.AbstractSchemeTest;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.chernysheva.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private ITaskService taskService = context.getBean(ITaskService.class);

    @NotNull
    private IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private List<Task> taskList;

    private static User USER_1;

    private static User USER_2;

    private static long USER_ID_COUNTER = 0;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_1 = userService.create("task_serv_usr_1_" + USER_ID_COUNTER, "1");
        USER_2 = userService.create("task_serv_usr_2_" + USER_ID_COUNTER, "1");
        taskList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_1_" + i);
            task.setDescription("Description_1_" + i);
            task.setUser(USER_1);
            taskList.add(task);
            taskService.add(USER_1.getId(), task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_2_" + i);
            task.setDescription("Description_2_" + i);
            task.setUser(USER_2);
            taskList.add(task);
            taskService.add(USER_2.getId(), task);
        }
    }

    @After
    public void closeConnection() {
        userService.clear();
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_1.getId()).size());
        taskService.clear(USER_1.getId());
        Assert.assertEquals(0, taskService.findAll(USER_1.getId()).size());
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_2.getId()).size());
        taskService.clear(USER_2.getId());
        Assert.assertEquals(0, taskService.findAll(USER_2.getId()).size());
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<Task> tasks = taskService.findAll(USER_1.getId());
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_1.getId()))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_2.getId());
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_2.getId()))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllSortNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULLABLE_USER_ID, CREATED_ENTITY_SORT));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID, CREATED_ENTITY_SORT));
    }

    @Test
    public void testFindAllSortPositive() {
        List<Task> tasks = taskService.findAll(USER_1.getId(), CREATED_ENTITY_SORT);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_1.getId()))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_1.getId(), NAME_ENTITY_SORT);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_1.getId()))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_1.getId(), STATUS_ENTITY_SORT);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_1.getId()))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_2.getId(), NULLABLE_ENTITY_SORT);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_2.getId()))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

    @Test
    public void testAddTaskNegative() {
        @NotNull final Task task = new Task();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(NULLABLE_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(EMPTY_USER_ID, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.add(USER_1.getId(), NULLABLE_TASK_MODEL));
    }

    @Test
    public void testAddTaskPositive() {
        @NotNull Task task = new Task();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        task.setUser(USER_1);
        taskService.add(USER_1.getId(), task);
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_1.getId()).size());
        task.setId(UUID.randomUUID().toString());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(NULLABLE_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(EMPTY_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_1.getId(), NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_1.getId(), EMPTY_TASK_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        for (final Task task : taskList) {
            Assert.assertTrue(taskService.existsById(task.getUser().getId(), task.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(NULLABLE_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(EMPTY_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_1.getId(), NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_1.getId(), EMPTY_TASK_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (final Task task : taskList) {
            Assert.assertEquals(task.getId(), taskService.findOneById(task.getUser().getId(), task.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(USER_1.getId(), NULLABLE_INDEX));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.findOneByIndex(USER_1.getId(), i + 1).getId());
        }
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.findOneByIndex(USER_2.getId(), i - 4).getId());
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(NULLABLE_USER_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(EMPTY_USER_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_1.getId(), NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_1.getId(), EMPTY_TASK_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final Task task : taskList) {
            taskService.removeById(task.getUser().getId(), task.getId());
            Assert.assertFalse(taskService.findAll(task.getUser().getId()).contains(task));
        }
        Assert.assertEquals(0, taskService.findAll(USER_1.getId()).size());
        Assert.assertEquals(0, taskService.findAll(USER_2.getId()).size());
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(USER_1.getId(), NULLABLE_INDEX));
    }

    @Test
    public void testCreateTaskNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(EMPTY_USER_ID, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_1.getId(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_1.getId(), "", null));
    }

    @Test
    public void testCreateTaskPositive() {
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_1.getId()).size());
        taskService.create(USER_1.getId(), "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_1.getId()).size());

        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_2.getId()).size());
        taskService.create(USER_2.getId(), "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_2.getId()).size());

        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_1.getId()).size());
        taskService.create(USER_1.getId(), "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.findAll(USER_1.getId()).size());

        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_2.getId()).size());
        taskService.create(USER_2.getId(), "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.findAll(USER_2.getId()).size());

        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.findAll(USER_1.getId()).size());
        taskService.create(USER_1.getId(), "PROJ_3", null);
        Assert.assertEquals(INIT_COUNT_TASKS + 3, taskService.findAll(USER_1.getId()).size());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(EMPTY_USER_ID, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_1.getId(), null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_1.getId(), "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusById(USER_1.getId(), UUID.randomUUID().toString(), null));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusById(USER_1.getId(), taskService.findOneByIndex(USER_1.getId(), 1).getId(), null));
    }

    @Test
    public void testChangeTaskStatusByIdPositive() {
        for (final Task task : taskList) {
            taskService.changeTaskStatusById(task.getUser().getId(), task.getId(), IN_PROGRESS_STATUS);
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findOneById(task.getUser().getId(), task.getId()).getStatus());
        }
    }

    @Test
    public void testChangeTaskStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(NULLABLE_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(EMPTY_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(USER_1.getId(), NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(USER_1.getId(), -1, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(UUID.randomUUID().toString(), 0, null));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusByIndex(USER_1.getId(), 1, null));
    }

    @Test
    public void testChangeTaskStatusByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            taskService.changeTaskStatusByIndex(USER_1.getId(), 1, IN_PROGRESS_STATUS);
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findAll(USER_1.getId()).get(i).getStatus());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            taskService.changeTaskStatusByIndex(USER_2.getId(), 1, IN_PROGRESS_STATUS);
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findAll(USER_2.getId()).get(i).getStatus());
        }
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(USER_1.getId(), null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(USER_1.getId(), "", null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_1.getId(), UUID.randomUUID().toString(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_1.getId(), UUID.randomUUID().toString(), "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateById(USER_1.getId(), UUID.randomUUID().toString(), "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIdPositive() {
        for (final Task task : taskList) {
            taskService.updateById(task.getUser().getId(), task.getId(), task.getName(), null);
            taskService.updateById(task.getUser().getId(), task.getId(), task.getName(), "");
            taskService.updateById(task.getUser().getId(), task.getId(), task.getName() + "_upd", task.getDescription() + "_upd");
            Assert.assertEquals(task.getId(), taskService.findOneById(task.getUser().getId(), task.getId()).getId());
            Assert.assertEquals(task.getName() + "_upd", taskService.findOneById(task.getUser().getId(), task.getId()).getName());
            Assert.assertEquals(task.getDescription() + "_upd", taskService.findOneById(task.getUser().getId(), task.getId()).getDescription());
        }
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(USER_1.getId(), null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(USER_1.getId(), -1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(USER_1.getId(), 1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(USER_1.getId(), 1, "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateByIndex(UUID.randomUUID().toString(), 1, "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Task task = taskList.get(i);
            taskService.updateByIndex(task.getUser().getId(), i + 1, task.getName(), null);
            taskService.updateByIndex(task.getUser().getId(), i + 1, task.getName(), "");
            taskService.updateByIndex(task.getUser().getId(), i + 1, task.getName() + "_upd", task.getDescription() + "_upd");
            task.setName(task.getName() + "_upd");
            Assert.assertNotNull(
                    taskService.findAll(task.getUser().getId()).stream()
                            .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                            .filter(m -> task.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Task task = taskList.get(i);
            taskService.updateByIndex(task.getUser().getId(), i - 4, task.getName(), null);
            taskService.updateByIndex(task.getUser().getId(), i - 4, task.getName(), "");
            taskService.updateByIndex(task.getUser().getId(), i - 4, task.getName() + "_upd", task.getDescription() + "_upd");
            task.setName(task.getName() + "_upd");
            Assert.assertNotNull(
                    taskService.findAll(task.getUser().getId()).stream()
                            .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                            .filter(m -> task.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testFindAllByProjectIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(NULLABLE_USER_ID, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(EMPTY_USER_ID, null));
    }

    @Test
    public void testFindAllByProjectIdPositive() throws Exception {
        Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(USER_1.getId(), null));
        Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(USER_1.getId(), ""));
        for (final Task task : taskList) {
            if (task.getProject() != null)
                Assert.assertNotNull(
                        taskService.findAllByProjectId(task.getUser().getId(), task.getProject().getId())
                                .stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

}
