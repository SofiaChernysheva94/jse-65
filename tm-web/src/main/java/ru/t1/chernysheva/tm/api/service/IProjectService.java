package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void save(@Nullable final ProjectDTO project);

    void saveAll(@Nullable final Collection<ProjectDTO> projects);

    void removeAll();

    void removeOneById(@Nullable final String id);

    void removeOne(@Nullable final ProjectDTO project);

    @NotNull
    List<ProjectDTO> findAll();

    @Nullable
    ProjectDTO findOneById(@Nullable final String id);

    @Nullable
    String getProjectNameById(@Nullable final String id);

}
