package ru.t1.chernysheva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.IProjectService;
import ru.t1.chernysheva.tm.exception.IdEmptyException;
import ru.t1.chernysheva.tm.exception.NameEmptyException;
import ru.t1.chernysheva.tm.model.ProjectDTO;
import ru.t1.chernysheva.tm.repository.IProjectRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @Override
    @Transactional
    public void save(@Nullable final ProjectDTO project) {
        if (project == null) throw new IdEmptyException();
        if (project.getName() == null || project.getName().isEmpty()) throw new NameEmptyException();
        repository.save(project);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final Collection<ProjectDTO> projects) {
        if (projects == null) throw new IdEmptyException();
        if (projects.isEmpty()) throw new IdEmptyException();
        for (@NotNull ProjectDTO projectDTO : projects)
            if (projectDTO.getName() == null || projectDTO.getName().isEmpty()) throw new NameEmptyException();
        repository.saveAll(projects);
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final ProjectDTO project) {
        if (project == null) throw new IdEmptyException();
        repository.delete(project);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

    @Nullable
    @Override
    public String getProjectNameById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get().getName();
    }

}
