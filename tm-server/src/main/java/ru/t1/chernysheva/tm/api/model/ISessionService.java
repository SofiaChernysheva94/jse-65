package ru.t1.chernysheva.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.model.Session;

import java.util.List;

public interface ISessionService {

    void clear(@Nullable final String userId);

    @NotNull
    List<Session> findAll(@Nullable final String userId);

    boolean existsById(@Nullable final String id);

    @Nullable
    Session findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    void remove(@NotNull Session session);

    void add(@Nullable String userId, @Nullable Session session);

    Session add(@NotNull Session session);

}
