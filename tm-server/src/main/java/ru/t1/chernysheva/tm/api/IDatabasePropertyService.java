package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.component.ISaltProvider;

public interface IDatabasePropertyService {

    @NotNull String getApplicationConfig();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUser();

    @NotNull
    String getDBSchema();

    @NotNull
    String getDBDriver();

    @NotNull String getDBL2Cache();

    @NotNull String getDBDialect();

    @NotNull String  getDBShowSQL();

    @NotNull String getDBHbm2DDL();

    @NotNull String getDBCacheRegion();

    @NotNull String getDBQueryCache();

    @NotNull String getDBMinimalPuts();

    @NotNull String getDBCacheRegionPrefix();

    @NotNull String getDBCacheProvider();
}
