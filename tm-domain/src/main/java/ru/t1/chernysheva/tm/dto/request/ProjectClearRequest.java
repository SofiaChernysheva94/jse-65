package ru.t1.chernysheva.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable final String token) {
        super(token);
    }

}
