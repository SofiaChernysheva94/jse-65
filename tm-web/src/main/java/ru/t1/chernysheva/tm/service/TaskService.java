package ru.t1.chernysheva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.ITaskService;
import ru.t1.chernysheva.tm.exception.IdEmptyException;
import ru.t1.chernysheva.tm.exception.NameEmptyException;
import ru.t1.chernysheva.tm.model.TaskDTO;
import ru.t1.chernysheva.tm.repository.ITaskRepository;

import java.util.Collection;
import java.util.List;

@Transactional
@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @Override
    @Transactional
    public void save(@Nullable final TaskDTO task) {
        if (task == null) throw new IdEmptyException();
        if (task.getName() == null || task.getName().isEmpty()) throw new NameEmptyException();
        repository.save(task);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final Collection<TaskDTO> tasks) {
        if (tasks == null) throw new IdEmptyException();
        if (tasks.isEmpty()) throw new IdEmptyException();
        for (@NotNull TaskDTO taskDTO : tasks)
            if (taskDTO.getName() == null || taskDTO.getName().isEmpty()) throw new NameEmptyException();
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final TaskDTO task) {
        if (task == null) throw new IdEmptyException();
        repository.delete(task);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

}
