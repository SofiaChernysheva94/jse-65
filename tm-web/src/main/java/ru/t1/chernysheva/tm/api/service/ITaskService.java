package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void save(@Nullable final TaskDTO task);

    void saveAll(@Nullable final Collection<TaskDTO> tasks);

    void removeAll();

    void removeOneById(@Nullable final String id);

    void removeOne(@Nullable final TaskDTO task);

    @NotNull
    List<TaskDTO> findAll();

    @Nullable
    TaskDTO findOneById(@Nullable final String id);

}
